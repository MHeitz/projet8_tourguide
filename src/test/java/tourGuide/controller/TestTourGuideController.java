package tourGuide.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.UserPreferencesDTO;
import tourGuide.service.GpsUtilService;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.service.impl.GpsUtilServiceImpl;
import tourGuide.user.User;
import tourGuide.user.UserReward;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class TestTourGuideController {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TourGuideService tourGuideService;

    @Autowired
    private RewardsService rewardsService;

    @Autowired
    private GpsUtil gpsUtil;



    @BeforeEach
    public void setUp(){

        String userName = "internalUser1";
        String phone = "000";
        String email = "internalUser1@tourGuide.com";
        User user = new User(UUID.randomUUID(), userName, phone, email);
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));

        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), new Location(100, 100), Date.from(localDateTime.toInstant(ZoneOffset.UTC)));
        user.addToVisitedLocations(visitedLocation);

        Map<String, User> internalUserMap = new HashMap<>();
        internalUserMap.put(userName, user);


        tourGuideService.setInternalUserMap(internalUserMap);
    }

    /**
     * Test to update user's preferences
     */
    @Test
    public void testToUpdateUserPreferences() throws Exception {

        InternalTestHelper.setInternalUserNumber(10);

        UserPreferencesDTO userPreferences = new UserPreferencesDTO();
        userPreferences.setNumberOfAdults(2);
        userPreferences.setNumberOfChildren(2);
        userPreferences.setTicketQuantity(4);
        userPreferences.setTripDuration(1);
        userPreferences.setCurrency("EUR");
        userPreferences.setHighPricePoint(300);
        userPreferences.setLowerPricePoint(30);




        mockMvc.perform(MockMvcRequestBuilders
                        .post("/postUserPreferences")
                        .param("userName", "internalUser1")
                        .content(asJsonString(userPreferences))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Test to get a trip, using the user's preferences
     */
    @Test
    public void testToGetTripUsingUserPreferences() throws Exception {

        this.testToUpdateUserPreferences();

        mockMvc.perform(get("/getTripDeals?userName=internalUser1"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").isNotEmpty());
    }

    /**
     * Test to get internalUser1's location
     */
    @Test
    public void testToGetUserLocation() throws Exception {

        mockMvc.perform(get("/getLocation?userName=internalUser1"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.longitude").isNotEmpty());
    }

    /**
     * Test to get rewards for the internalUser1
     */

    @Test
    public void testToGetRewards() throws Exception {

        User user = tourGuideService.getUser("internalUser1");
        Location location = new Location(10.0, 10.0);
        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), location, new Date());
        Attraction attraction = gpsUtil.getAttractions().get(0);
        user.addToVisitedLocations(visitedLocation);
        UserReward userReward = new UserReward(visitedLocation, attraction, 1000);
        user.addUserReward(userReward);

        mockMvc.perform(get("/getRewards?userName=internalUser1"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].rewardPoints").value(1000));
    }

    /**
     * Test to get all user's location
     */
    @Test
    public void testToGetAllUserLocation() throws Exception {

        mockMvc.perform(get("/getAllCurrentLocations"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty());
    }

    /**
     * Test to get the 5 nearby attractions from user location
     */
    @Test
    public void testToGetNearbyAttractions() throws Exception {


        mockMvc.perform(get("/getNearbyAttractions")
                        .param("userName", "internalUser1"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.attractionDTO").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.user.userName").value("internalUser1"));


    }

}


