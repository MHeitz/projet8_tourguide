package tourGuide;


import java.util.*;

import gpsUtil.location.Location;
import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;


import gpsUtil.GpsUtil;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.AttractionDTO;
import tourGuide.model.AttractionWithDistanceFromUser;

import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.service.impl.GpsUtilServiceImpl;
import tourGuide.user.User;
import tripPricer.Provider;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@Slf4j
public class TestTourGuideService {


	@MockBean
	private GpsUtilServiceImpl gpsUtilService;

	@Autowired
	private TourGuideService tourGuideService;


	public AttractionWithDistanceFromUser generateNearbyAttractionList(){

		AttractionDTO attractionDTO1 = new AttractionDTO();
		attractionDTO1.setAttractionName("test1");
		attractionDTO1.setLatitude(42.532026);
		attractionDTO1.setLongitude(1.642342);

		AttractionDTO attractionDTO2 = new AttractionDTO();
		attractionDTO2.setAttractionName("test2");
		attractionDTO2.setLatitude(43.532026);
		attractionDTO2.setLongitude(2.642342);

		AttractionDTO attractionDTO3 = new AttractionDTO();
		attractionDTO3.setAttractionName("test3");
		attractionDTO3.setLatitude(44.532026);
		attractionDTO3.setLongitude(3.642342);

		AttractionDTO attractionDTO4 = new AttractionDTO();
		attractionDTO4.setAttractionName("test4");
		attractionDTO4.setLatitude(45.532026);
		attractionDTO4.setLongitude(4.642342);

		AttractionDTO attractionDTO5 = new AttractionDTO();
		attractionDTO5.setAttractionName("test5");
		attractionDTO5.setLatitude(46.532026);
		attractionDTO5.setLongitude(4.642342);

		AttractionWithDistanceFromUser attractionWithDistanceFromUser = new AttractionWithDistanceFromUser();

		attractionDTO1.setDistanceFromUser(10);

		attractionDTO2.setDistanceFromUser(11);

		attractionDTO3.setDistanceFromUser(12);

		attractionDTO4.setDistanceFromUser(14);

		attractionDTO5.setDistanceFromUser(16);

		List<AttractionDTO> list = new ArrayList<>();
		list.add(attractionDTO1);
		list.add(attractionDTO2);
		list.add(attractionDTO3);
		list.add(attractionDTO4);
		list.add(attractionDTO5);

		attractionWithDistanceFromUser.setAttractionDTO(list);

		return attractionWithDistanceFromUser;
	}


	@Test
	public void getUserLocation() throws InterruptedException {

		InternalTestHelper.setInternalUserNumber(0);

		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		Location userLocation = new Location(-10, 610);

		VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), userLocation, new Date() );

		when(gpsUtilService.getUserLocation(user.getUserId())).thenReturn(visitedLocation);

		List<User> list = new ArrayList<>();
		list.add(user);

		tourGuideService.trackUserLocation(list);

		VisitedLocation visitedLocationExpected = tourGuideService.getUserLocation(user);

		tourGuideService.tracker.stopTracking();
		assertEquals(visitedLocation.userId, user.getUserId());
		assertEquals(visitedLocationExpected.location, userLocation);
	}


	@Test
	public void addUser() {
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);

		User retrivedUser = tourGuideService.getUser(user.getUserName());
		User retrivedUser2 = tourGuideService.getUser(user2.getUserName());

		tourGuideService.tracker.stopTracking();

		assertEquals(user, retrivedUser);
		assertEquals(user2, retrivedUser2);
	}

	@Test
	public void getAllUsers() {
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);

		List<User> allUsers = tourGuideService.getAllUsers();

		tourGuideService.tracker.stopTracking();

		assertTrue(allUsers.contains(user));
		assertTrue(allUsers.contains(user2));
	}

	@Test
	public void testTrackUser() throws InterruptedException {

		InternalTestHelper.setInternalUserNumber(1);

		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		Location userLocation = new Location(-10, 610);

		VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), userLocation, new Date() );

		when(gpsUtilService.getUserLocation(user.getUserId())).thenReturn(visitedLocation);

		List<User> list = new ArrayList<>();
		list.add(user);

		tourGuideService.trackUserLocation(list);

		VisitedLocation userVisitedLocation = tourGuideService.getUserLocation(user);

		tourGuideService.tracker.stopTracking();

		assertEquals(user.getUserId(), userVisitedLocation.userId);
		assertNotNull(userVisitedLocation);
	}


	/**
	 * Test to get the 5 nearby attractions, and their distance from a user
	 */
	@Test
	public void testToGetNearbyAttractions() {

		AttractionWithDistanceFromUser list = this.generateNearbyAttractionList();

		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		Location location = new Location(42.532026, 1.642342);
		VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), location, new Date());
		list.setUser(user);

		when(gpsUtilService.getNearbyAttraction(visitedLocation.location, 5, user)).thenReturn(list);

		AttractionWithDistanceFromUser nearbyAttractions = tourGuideService.getNearByAttractions(visitedLocation, 5, user);

		assertEquals(nearbyAttractions.getAttractionDTO().size(), 5);
	}

	@Test
	public void getTripDeals() {
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

		List<Provider> providers = tourGuideService.getTripDeals(user);

		tourGuideService.tracker.stopTracking();

		assertEquals(5, providers.size());
	}

	@Test
	public void getAllUserLocations(){

		//set up
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		InternalTestHelper.setInternalUserNumber(2);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

		Map<String, Location> usersLocations = tourGuideService.getAllUserLocations();

		assertEquals(usersLocations.size(), 2);

	}


}
