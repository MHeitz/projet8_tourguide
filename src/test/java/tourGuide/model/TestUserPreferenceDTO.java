package tourGuide.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TestUserPreferenceDTO {

    @Test
    public void testHashCode()
    {
        UserPreferencesDTO userPreferencesDTO = new UserPreferencesDTO();
        userPreferencesDTO.setAttractionProximity(10);
        userPreferencesDTO.setCurrency("EUR");
        userPreferencesDTO.setLowerPricePoint(30);
        userPreferencesDTO.setHighPricePoint(300);
        userPreferencesDTO.setTicketQuantity(4);
        userPreferencesDTO.setNumberOfAdults(2);
        userPreferencesDTO.setNumberOfChildren(3);
        userPreferencesDTO.setTripDuration(1);


        assertNotNull(userPreferencesDTO.hashCode());

    }

    @Test
    public void testEquals()
    {
        UserPreferencesDTO userPreferencesDTO = new UserPreferencesDTO();
        userPreferencesDTO.setAttractionProximity(10);
        userPreferencesDTO.setCurrency("EUR");
        userPreferencesDTO.setLowerPricePoint(30);
        userPreferencesDTO.setHighPricePoint(300);
        userPreferencesDTO.setTicketQuantity(4);
        userPreferencesDTO.setNumberOfAdults(2);
        userPreferencesDTO.setNumberOfChildren(3);
        userPreferencesDTO.setTripDuration(1);

        UserPreferencesDTO userPreferencesDTO2 = new UserPreferencesDTO();
        userPreferencesDTO2.setAttractionProximity(10);
        userPreferencesDTO2.setCurrency("EUR");
        userPreferencesDTO2.setLowerPricePoint(30);
        userPreferencesDTO2.setHighPricePoint(300);
        userPreferencesDTO2.setTicketQuantity(4);
        userPreferencesDTO2.setNumberOfAdults(2);
        userPreferencesDTO2.setNumberOfChildren(3);
        userPreferencesDTO2.setTripDuration(1);

        UserPreferencesDTO userPreferencesDTO3 = new UserPreferencesDTO();
        userPreferencesDTO3.setAttractionProximity(10);
        userPreferencesDTO3.setCurrency("EUR");
        userPreferencesDTO3.setLowerPricePoint(30);
        userPreferencesDTO3.setHighPricePoint(300);
        userPreferencesDTO3.setTicketQuantity(4);
        userPreferencesDTO3.setNumberOfAdults(2);
        userPreferencesDTO3.setNumberOfChildren(3);
        userPreferencesDTO3.setTripDuration(3);


        assertTrue(userPreferencesDTO.equals(userPreferencesDTO));

        assertTrue(userPreferencesDTO.equals(userPreferencesDTO2));

        userPreferencesDTO2 = null;

        assertFalse(userPreferencesDTO.equals(userPreferencesDTO2));


        assertFalse(userPreferencesDTO.equals(userPreferencesDTO3));

        userPreferencesDTO3.setTripDuration(1);

        userPreferencesDTO3.setNumberOfChildren(1);

        assertFalse(userPreferencesDTO.equals(userPreferencesDTO3));
    }

    @Test
    public void testToString()
    {
        UserPreferencesDTO userPreferencesDTO = new UserPreferencesDTO();
        userPreferencesDTO.setAttractionProximity(10);
        userPreferencesDTO.setCurrency("EUR");
        userPreferencesDTO.setLowerPricePoint(30);
        userPreferencesDTO.setHighPricePoint(300);
        userPreferencesDTO.setTicketQuantity(4);
        userPreferencesDTO.setNumberOfAdults(2);
        userPreferencesDTO.setNumberOfChildren(3);
        userPreferencesDTO.setTripDuration(1);

        assertNotNull(userPreferencesDTO.toString());
    }

}
