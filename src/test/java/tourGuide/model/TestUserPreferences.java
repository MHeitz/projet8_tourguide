package tourGuide.model;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;

import javax.money.Monetary;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TestUserPreferences {

    @Test
    public void testHashCode()
    {
        UserPreferences userPreferences = new UserPreferences();
        userPreferences.setNumberOfAdults(2);
        userPreferences.setNumberOfChildren(2);
        userPreferences.setTicketQuantity(4);
        userPreferences.setTripDuration(1);
        userPreferences.setCurrency(Monetary.getCurrency("EUR"));
        userPreferences.setHighPricePoint(Money.of(300, userPreferences.getCurrency()));
        userPreferences.setLowerPricePoint(Money.of(30, userPreferences.getCurrency()));


        assertNotNull(userPreferences.hashCode());

    }

    @Test
    public void testEquals()
    {
        UserPreferences userPreferences = new UserPreferences();
        userPreferences.setNumberOfAdults(2);
        userPreferences.setNumberOfChildren(2);
        userPreferences.setTicketQuantity(4);
        userPreferences.setTripDuration(1);
        userPreferences.setCurrency(Monetary.getCurrency("EUR"));
        userPreferences.setHighPricePoint(Money.of(300, userPreferences.getCurrency()));
        userPreferences.setLowerPricePoint(Money.of(30, userPreferences.getCurrency()));

        UserPreferences userPreferences2 = new UserPreferences();
        userPreferences.setNumberOfAdults(2);
        userPreferences.setNumberOfChildren(2);
        userPreferences.setTicketQuantity(4);
        userPreferences.setTripDuration(1);
        userPreferences.setCurrency(Monetary.getCurrency("EUR"));
        userPreferences.setHighPricePoint(Money.of(300, userPreferences.getCurrency()));
        userPreferences.setLowerPricePoint(Money.of(30, userPreferences.getCurrency()));

        UserPreferences userPreferences3 = new UserPreferences();
        userPreferences.setNumberOfAdults(4);
        userPreferences.setNumberOfChildren(2);
        userPreferences.setTicketQuantity(4);
        userPreferences.setTripDuration(1);
        userPreferences.setCurrency(Monetary.getCurrency("EUR"));
        userPreferences.setHighPricePoint(Money.of(300, userPreferences.getCurrency()));
        userPreferences.setLowerPricePoint(Money.of(30, userPreferences.getCurrency()));

        assertTrue(userPreferences.equals(userPreferences));


        assertFalse(userPreferences.equals(userPreferences2));


        assertFalse(userPreferences.equals(userPreferences3));

        userPreferences3.setNumberOfAdults(2);

        userPreferences3.setNumberOfChildren(4);

        assertFalse(userPreferences.equals(userPreferences3));
    }

    @Test
    public void testToString()
    {
        UserPreferences userPreferences = new UserPreferences();
        userPreferences.setNumberOfAdults(2);
        userPreferences.setNumberOfChildren(2);
        userPreferences.setTicketQuantity(4);
        userPreferences.setTripDuration(1);
        userPreferences.setCurrency(Monetary.getCurrency("EUR"));
        userPreferences.setHighPricePoint(Money.of(300, userPreferences.getCurrency()));
        userPreferences.setLowerPricePoint(Money.of(30, userPreferences.getCurrency()));

        assertNotNull(userPreferences.toString());
    }
}
