package tourGuide.model;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.junit.jupiter.api.Test;
import tourGuide.user.User;

import java.util.UUID;


import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TestUser {


    @Test
    public void testHashCode()
    {
        User user = new User(UUID.randomUUID(), "testUser", "testPhoneNumber", "testEmail");


        assertNotNull(user.hashCode());

    }

    @Test
    public void testEquals()
    {
        UUID uuid = UUID.randomUUID();
        User user = new User(uuid, "testUser", "testPhoneNumber", "testEmail");

        User user2 = user;

        User user3 = new User(uuid, "testUser", "testPhoneNumber3", "testEmail");
        user3.setUserPreferences(user.getUserPreferences());

        assertTrue(user.equals(user));

        assertTrue(user.equals(user2));

        user2 = null;

        assertFalse(user.equals(user2));


        assertFalse(user.equals(user3));

        user3.setPhoneNumber("testPhoneNumber");

        user3.setEmailAddress("TestEmail3");

        assertFalse(user.equals(user3));
    }

    @Test
    public void testToString()
    {
        User user = new User(UUID.randomUUID(), "testUser", "testPhoneNumber", "testEmail");
        assertNotNull(user.toString());
    }

}
