package tourGuide.model;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import tourGuide.user.User;
import tourGuide.user.UserReward;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TestUserReward {


    /**
     * Test to set rewards to a user
     */
    @Test
    public void testToSetRewards(){

        UUID uuid = UUID.randomUUID();
        User user = new User(uuid, "userTest", "000000", "test@gmail.com");
        Location location = new Location(10.0, 10.0);
        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), location, new Date());
        user.addToVisitedLocations(visitedLocation);
        Attraction attraction = new Attraction("testattraction", "cityTest", "satteTest", 10.0, 10.0);

        UserReward userReward = new UserReward(user.getLastVisitedLocation(), attraction);

        userReward.setRewardPoints(10);

        assertEquals(userReward.getRewardPoints(), 10);
    }
}
