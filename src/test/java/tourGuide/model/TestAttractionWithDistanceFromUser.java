package tourGuide.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import tourGuide.user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TestAttractionWithDistanceFromUser {

    @Test
    public void testHashCode()
    {
        AttractionDTO attractionDTO = new AttractionDTO();
        attractionDTO.setRewardPoints(10);
        attractionDTO.setAttractionName("testAttractionDTO");
        attractionDTO.setLongitude(10.0);
        attractionDTO.setLatitude(10.0);
        attractionDTO.setDistanceFromUser(100);

        List<AttractionDTO> list = new ArrayList<>();
        list.add(attractionDTO);

        User user = new User(UUID.randomUUID(), "testUser", "testPhoneNumber", "testEmail");

        AttractionWithDistanceFromUser attraction = new AttractionWithDistanceFromUser();
        attraction.setAttractionDTO(list);
        attraction.setUser(user);


        assertNotNull(attraction.hashCode());

    }

    @Test
    public void testEquals()
    {
        AttractionDTO attractionDTO = new AttractionDTO();
        attractionDTO.setRewardPoints(10);
        attractionDTO.setAttractionName("testAttractionDTO");
        attractionDTO.setLongitude(10.0);
        attractionDTO.setLatitude(10.0);
        attractionDTO.setDistanceFromUser(100);

        List<AttractionDTO> list = new ArrayList<>();
        list.add(attractionDTO);

        AttractionDTO attractionDTO3 = new AttractionDTO();
        attractionDTO3.setRewardPoints(10);
        attractionDTO3.setAttractionName("testAttractionDTO");
        attractionDTO3.setLongitude(10.0);
        attractionDTO3.setLatitude(10.0);
        attractionDTO3.setDistanceFromUser(200);

        List<AttractionDTO> list3 = new ArrayList<>();
        list.add(attractionDTO3);


        AttractionWithDistanceFromUser attraction = new AttractionWithDistanceFromUser();
        attraction.setAttractionDTO(list);

        AttractionWithDistanceFromUser attraction2 = new AttractionWithDistanceFromUser();
        attraction2.setAttractionDTO(list);

        AttractionWithDistanceFromUser attraction3 = new AttractionWithDistanceFromUser();
        attraction3.setAttractionDTO(list3);

        assertTrue(attraction.equals(attraction));

        assertTrue(attraction.equals(attraction2));

        attraction2 = null;

        assertFalse(attraction.equals(attraction2));


        assertFalse(attraction.equals(attraction3));

        attraction3.setAttractionDTO(list);


        assertTrue(attraction.equals(attraction3));
    }

    @Test
    public void testToString()
    {
        AttractionDTO attractionDTO = new AttractionDTO();
        attractionDTO.setRewardPoints(10);
        attractionDTO.setAttractionName("testAttractionDTO");
        attractionDTO.setLongitude(10.0);
        attractionDTO.setLatitude(10.0);
        attractionDTO.setDistanceFromUser(100);

        List<AttractionDTO> list = new ArrayList<>();
        list.add(attractionDTO);

        User user = new User(UUID.randomUUID(), "testUser", "testPhoneNumber", "testEmail");

        AttractionWithDistanceFromUser attraction = new AttractionWithDistanceFromUser();
        attraction.setAttractionDTO(list);
        attraction.setUser(user);

        assertNotNull(attraction.toString());
    }
}
