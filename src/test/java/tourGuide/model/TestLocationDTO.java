package tourGuide.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TestLocationDTO {

    @Test
    public void testHashCode()
    {
        LocationDTO locationDTO = new LocationDTO();
        locationDTO.setLatitude(10);
        locationDTO.setLongitude(10);


        assertNotNull(locationDTO.hashCode());

    }

    @Test
    public void testEquals()
    {
        LocationDTO locationDTO = new LocationDTO();
        locationDTO.setLatitude(10);
        locationDTO.setLongitude(10);

        LocationDTO locationDTO2 = new LocationDTO();
        locationDTO2.setLatitude(10);
        locationDTO2.setLongitude(10);

        LocationDTO locationDTO3 = new LocationDTO();
        locationDTO3.setLatitude(10);
        locationDTO3.setLongitude(20);


        assertTrue(locationDTO.equals(locationDTO));

        assertTrue(locationDTO.equals(locationDTO2));

        locationDTO2 = null;

        assertFalse(locationDTO.equals(locationDTO2));


        assertFalse(locationDTO.equals(locationDTO3));

        locationDTO3.setLongitude(10);

        locationDTO3.setLatitude(22);

        assertFalse(locationDTO.equals(locationDTO3));
    }

    @Test
    public void testToString()
    {
        LocationDTO locationDTO = new LocationDTO();
        locationDTO.setLatitude(10);
        locationDTO.setLongitude(10);

        assertNotNull(locationDTO.toString());
    }
}
