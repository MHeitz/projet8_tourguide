package tourGuide.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TestAttractionDTO {

    @Test
    public void testHashCode()
    {
        AttractionDTO attractionDTO = new AttractionDTO();
        attractionDTO.setRewardPoints(10);
        attractionDTO.setAttractionName("testAttractionDTO");
        attractionDTO.setLongitude(10.0);
        attractionDTO.setLatitude(10.0);
        attractionDTO.setDistanceFromUser(100);


        assertNotNull(attractionDTO.hashCode());

    }

    @Test
    public void testEquals()
    {
        AttractionDTO attractionDTO = new AttractionDTO();
        attractionDTO.setRewardPoints(10);
        attractionDTO.setAttractionName("testAttractionDTO");
        attractionDTO.setLongitude(10.0);
        attractionDTO.setLatitude(10.0);
        attractionDTO.setDistanceFromUser(100);

        AttractionDTO attractionDTO2 = new AttractionDTO();
        attractionDTO2.setRewardPoints(10);
        attractionDTO2.setAttractionName("testAttractionDTO");
        attractionDTO2.setLongitude(10.0);
        attractionDTO2.setLatitude(10.0);
        attractionDTO2.setDistanceFromUser(100);

        AttractionDTO attractionDTO3 = new AttractionDTO();
        attractionDTO3.setRewardPoints(20);
        attractionDTO3.setAttractionName("testAttractionDTO");
        attractionDTO3.setLongitude(10.0);
        attractionDTO3.setLatitude(10.0);
        attractionDTO3.setDistanceFromUser(100);

        assertTrue(attractionDTO.equals(attractionDTO));

        assertTrue(attractionDTO.equals(attractionDTO2));

        attractionDTO2 = null;

        assertFalse(attractionDTO.equals(attractionDTO2));


        assertFalse(attractionDTO.equals(attractionDTO3));

        attractionDTO3.setRewardPoints(10);

        attractionDTO3.setLatitude(22);

        assertFalse(attractionDTO.equals(attractionDTO3));
    }

    @Test
    public void testToString()
    {
        AttractionDTO attractionDTO = new AttractionDTO();
        attractionDTO.setRewardPoints(10);
        attractionDTO.setAttractionName("testAttractionDTO");
        attractionDTO.setLongitude(10.0);
        attractionDTO.setLatitude(10.0);
        attractionDTO.setDistanceFromUser(100);

        assertNotNull(attractionDTO.toString());
    }
}
