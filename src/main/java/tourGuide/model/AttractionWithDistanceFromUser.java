package tourGuide.model;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.Getter;
import tourGuide.user.User;

import java.util.List;
import java.util.Objects;

@Data
public class AttractionWithDistanceFromUser {


    public List<AttractionDTO> getAttractionDTO() {
        return attractionDTO;
    }

    public User getUser() {
        return user;
    }

    public void setAttractionDTO(List<AttractionDTO> attractionDTO) {
        this.attractionDTO = attractionDTO;
    }

    @Getter
    @JsonView(View.NearbyAttractionFilter.class)
    List<AttractionDTO> attractionDTO;

    public void setUser(User user) {
        this.user = user;
    }

    @JsonView(View.UserFilter.class)
    User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttractionWithDistanceFromUser attraction = (AttractionWithDistanceFromUser) o;
        return attractionDTO == attraction.attractionDTO && user == attraction.user;
    }

    @Override
    public int hashCode() {
        return Objects.hash(attractionDTO, user);
    }

}
