package tourGuide.model;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import java.util.Objects;


/**
 * Class replacing the former object Attraction from the library GpsUtil.
 *
 */
@Data
public class AttractionDTO {
    public void setAttractionName(String attractionName) {
        this.attractionName = attractionName;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setRewardPoints(int rewardPoints) {
        this.rewardPoints = rewardPoints;
    }

    @JsonView(View.NearbyAttractionFilter.class)
    private String attractionName;

    public void setDistanceFromUser(double distanceFromUser) {
        this.distanceFromUser = distanceFromUser;
    }

    @JsonView(View.NearbyAttractionFilter.class)
    double distanceFromUser;

    @JsonView(View.NearbyAttractionFilter.class)
    private double latitude;

    @JsonView(View.NearbyAttractionFilter.class)
    private double longitude;

    @JsonView(View.NearbyAttractionFilter.class)
    private int rewardPoints;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttractionDTO attractionDTO = (AttractionDTO) o;
        return attractionName == attractionDTO.attractionName && distanceFromUser == attractionDTO.distanceFromUser && Objects.equals(latitude, attractionDTO.latitude) && Objects.equals(longitude, attractionDTO.longitude) && Objects.equals(rewardPoints, attractionDTO.rewardPoints);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attractionName, distanceFromUser, latitude, longitude, rewardPoints);
    }

}
