package tourGuide.model;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

import java.util.Objects;

@Data
public class LocationDTO {

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @JsonView(View.UserFilter.class)
    private double latitude;

    @JsonView(View.UserFilter.class)
    private double longitude;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationDTO locationDTO = (LocationDTO) o;
        return latitude == locationDTO.latitude && longitude == locationDTO.longitude;
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }
}
