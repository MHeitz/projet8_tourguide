package tourGuide.service.impl;

import gpsUtil.location.Attraction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tourGuide.model.AttractionDTO;
import tourGuide.service.AttractionDTOService;
import tourGuide.service.RewardsService;
import tourGuide.user.User;


@Service
public class AttractionDTOServiceImpl implements AttractionDTOService {

    @Autowired
    RewardsService rewardsService;

    private static final Logger logger = LogManager.getLogger("AttractionDTOService");

    @Override
    public AttractionDTO convertAttractionToAttractionDTO(Attraction attraction, User user) {

        AttractionDTO attractionDTO = new AttractionDTO();

        try{

            attractionDTO.setAttractionName(attraction.attractionName);
            attractionDTO.setLatitude(attraction.latitude);
            attractionDTO.setLongitude(attraction.longitude);
            attractionDTO.setRewardPoints(rewardsService.getRewardPoints(attraction, user));


            logger.debug("The attraction was converted into attractionDTO with success");
        }
        catch (Exception ex){

            logger.error("The attraction wasn't converted into attractionDTO");
        }
        return attractionDTO;
    }


}
