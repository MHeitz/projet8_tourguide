package tourGuide.service.impl;

import com.google.common.util.concurrent.RateLimiter;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tourGuide.model.AttractionDTO;
import tourGuide.model.AttractionWithDistanceFromUser;
import tourGuide.service.AttractionDTOService;
import tourGuide.service.GpsUtilService;
import tourGuide.user.User;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@Service
public class GpsUtilServiceImpl implements GpsUtilService {

    private static final RateLimiter rateLimiter = RateLimiter.create(1000.0);

    @Autowired
    private GpsUtil gpsUtil;

    @Autowired
    private AttractionDTOService attractionDTOService;

    private static final Logger logger = LogManager.getLogger("GpsUtilServiceImpl");

    @Override
    public double convertDegreesToRadians(double degrees)
    {
        double radius = 0;

        try {
            radius = degrees * (Math.PI / 180);

            logger.debug("The Point degree was converted to radius");
        }
        catch (Exception ex)
        {
            logger.error("Error to convert the point degree in radius");
        }

        return  radius;
    }

    @Override
    public double calculateDistanceBetweenTwoPoints(Attraction attraction, Location location)
    {
        double distance =0;

        try {
            // Radius in Km
            double radiusEarth = 6371;

            // Convert degrees to radians
            double radiusLatitudeAttraction = this.convertDegreesToRadians(attraction.latitude);
            double radiusLatitudeLocation = this.convertDegreesToRadians(location.latitude);
            double radiusLongitudeAttraction = this.convertDegreesToRadians(attraction.longitude);
            double radiusLongitudeLocation = this.convertDegreesToRadians(location.longitude);



            distance = Math.acos(Math.sin(radiusLatitudeLocation) * Math.sin(radiusLatitudeAttraction) + Math.cos(radiusLatitudeLocation)* Math.cos(radiusLatitudeAttraction) * Math.cos(radiusLongitudeLocation - radiusLongitudeAttraction))*radiusEarth;

            distance = distance/0.621371;

            logger.debug("The distance between these two points was calculated");
        }
        catch (Exception ex){

            logger.error("Error to calculate the distance between these two points");
        }

        return distance;
    }
    @Override
    public AttractionWithDistanceFromUser getNearbyAttraction(Location userLocation, int numberOfAttractionWanted, User user) {

        HashMap<Attraction, Double> listOfAttractionsAndDistanceOfUser = new HashMap<>();
        List<AttractionDTO> sorteList = new ArrayList<>();
        AttractionWithDistanceFromUser attractionWithDistanceFromUser = new AttractionWithDistanceFromUser();
        ArrayList<Double> listOfDistance = new ArrayList<>();

        try{
            List<Attraction> listOfAllAttraction = gpsUtil.getAttractions();

            //calculate the distance between the attraction and the user location
            for(Attraction attraction : listOfAllAttraction)
            {
                double distance = this.calculateDistanceBetweenTwoPoints(attraction, userLocation);
                listOfAttractionsAndDistanceOfUser.put(attraction, distance);
                listOfDistance.add(distance);
            }
            Collections.sort(listOfDistance);

            //insert in list the nearby attractions according to the number of attraction wanted
            for (int i=0; i< numberOfAttractionWanted; i++) {
                for (Map.Entry<Attraction, Double> entry : listOfAttractionsAndDistanceOfUser.entrySet()) {
                    double distance = listOfDistance.get(i);
                    if (entry.getValue().equals(distance)) {
                        AttractionDTO attractionDTO = attractionDTOService.convertAttractionToAttractionDTO(entry.getKey(), user);
                        attractionDTO.setDistanceFromUser(distance);
                        sorteList.add(attractionDTO);
                        listOfAttractionsAndDistanceOfUser.remove(entry);
                    }
                }
            }
            attractionWithDistanceFromUser.setAttractionDTO(sorteList);
            logger.debug("The list of nearby attraction was fetched");
        }
        catch (Exception ex) {

            logger.error("Error to get the nearby attractions");
        }
        return attractionWithDistanceFromUser;
    }


    /**
     * Obtains the user's location.
     * This Method supersedes the GpsUtil's method getUserLocation(), which didn't works,
     * due to an error of strong conversion of latitude and longitude
     *
     * @param userId   - The user's Id)
     * @return - Visited location Object
     */

    @Override
    public VisitedLocation getUserLocation(UUID userId) {
        rateLimiter.acquire();
        this.sleep();
        double longitude = ThreadLocalRandom.current().nextDouble(-180.0, 180.0);
        String longstr = String.format("%.6f", longitude);
        longstr = longstr.replace(',','.');
        longitude = Double.parseDouble(longstr);
        double latitude = ThreadLocalRandom.current().nextDouble(-85.05112878, 85.05112878);
        String latstr = String.format("%.6f", latitude);
        latstr = latstr.replace(',','.');
        latitude = Double.parseDouble(latstr);
        return new VisitedLocation(userId, new Location(latitude, longitude), new Date());

    }

    @Override
    public void sleep() {
        int random = ThreadLocalRandom.current().nextInt(30, 100);

        try {
            TimeUnit.MILLISECONDS.sleep(random);
        } catch (InterruptedException var3) {
        }

    }
}
