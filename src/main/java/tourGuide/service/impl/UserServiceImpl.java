package tourGuide.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tourGuide.model.UserPreferencesDTO;
import tourGuide.service.TourGuideService;
import tourGuide.service.UserService;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;

import javax.money.Monetary;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger = LogManager.getLogger("UserService");

    @Autowired
    TourGuideService tourGuideService;


    /**
     * Add to a user, his preferences
     *
     * @param userPreferencesDTO   - The user's preferences
     * @param userName - the user's Name
     * @return - boolean (if the methode was correctly done or not
     */

    public boolean saveNewUserPreference(UserPreferencesDTO userPreferencesDTO, String userName){
        boolean result = false;
        try{
            User user = tourGuideService.getUser(userName);

            UserPreferences userPreferences = new UserPreferences();
            userPreferences.setTicketQuantity(userPreferencesDTO.getTicketQuantity());
            userPreferences.setNumberOfAdults(userPreferencesDTO.getNumberOfAdults());
            userPreferences.setNumberOfChildren(userPreferencesDTO.getNumberOfChildren());
            userPreferences.setLowerPricePoint(Money.of(userPreferencesDTO.getLowerPricePoint(), userPreferences.getCurrency()));
            userPreferences.setHighPricePoint(Money.of(userPreferencesDTO.getHighPricePoint(), userPreferences.getCurrency()));
            userPreferences.setCurrency(Monetary.getCurrency(userPreferencesDTO.getCurrency()));
            user.setUserPreferences(userPreferences);

            result = true;
            logger.debug("The user's Preferences were update");
        }
        catch (Exception ex){
            logger.error("Error to update user's preferences");
        }
        return result;
    }

}
