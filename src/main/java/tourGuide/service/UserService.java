package tourGuide.service;

import tourGuide.model.UserPreferencesDTO;

public interface UserService {

    boolean saveNewUserPreference(UserPreferencesDTO userPreferences, String userName);
}
