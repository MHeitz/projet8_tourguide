package tourGuide.service;

import gpsUtil.location.Attraction;
import tourGuide.model.AttractionDTO;
import tourGuide.user.User;

public interface AttractionDTOService {

    AttractionDTO convertAttractionToAttractionDTO(Attraction attraction, User user);
}
