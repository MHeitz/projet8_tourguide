package tourGuide.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;


import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.AttractionWithDistanceFromUser;
import tourGuide.service.impl.GpsUtilServiceImpl;
import tourGuide.tracker.Tracker;
import tourGuide.user.User;
import tourGuide.user.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

@Service
public class TourGuideService{
	private Logger logger = LoggerFactory.getLogger(TourGuideService.class);

	@Autowired
	private GpsUtilServiceImpl gpsUtilService;


	private final GpsUtil gpsUtil;
	private final RewardsService rewardsService;
	private final TripPricer tripPricer = new TripPricer();
	public final Tracker tracker;
	boolean testMode = true;

	public TourGuideService(GpsUtil gpsUtil, RewardsService rewardsService) {
		this.gpsUtil = gpsUtil;
		this.rewardsService = rewardsService;

		if(testMode) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this);
		addShutDownHook();
	}

	public List<UserReward> getUserRewards(User user) {
		return user.getUserRewards();
	}


	public VisitedLocation getUserLocation(User user) {
		VisitedLocation visitedLocation = null;

		try{
			visitedLocation = (user.getVisitedLocations().size() > 0) ?
					user.getLastVisitedLocation() :
					gpsUtilService.getUserLocation(user.getUserId());
			logger.info("The user location was find");

		}
		catch (Exception e){
			logger.error("the User location wasn't find");
		}

		return visitedLocation;
	}

	public User getUser(String userName) {

		User user = null;

		try{
			user = internalUserMap.get(userName);
			logger.info("The user was find");
		}
		catch (Exception ex){
			logger.error("User not find");
		}
		return user;
	}

	public List<User> getAllUsers() {
		return internalUserMap.values().stream().collect(Collectors.toList());
	}

	public void addUser(User user) {
		if(!internalUserMap.containsKey(user.getUserName())) {
			internalUserMap.put(user.getUserName(), user);
		}
	}

	/*
	*Modification of the methode. Addition of a condition to control if the user's preferences were saved
	*
	 */
	public List<Provider> getTripDeals(User user) {

		List<Provider> providers= null;

		if(user.getUserPreferences() != null){
			int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
			providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
					user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
			user.setTripDeals(providers);
			logger.info("The providers were fetch");
			return providers;
		}
		else{
			logger.error("Update the user's preferences is necessary");
		}
		return providers;
	}


	public void trackUserLocation(List<User> users) throws InterruptedException {

		//Allowed to run pool of 100 threads
		final ExecutorService executor = Executors.newFixedThreadPool(100);

		List<Callable<VisitedLocation>> tasks = new ArrayList<Callable<VisitedLocation>>();

		for (User user : users) {

			tasks.add(new Callable<VisitedLocation>() {
				public VisitedLocation call() throws Exception {
					VisitedLocation visitedLocation = gpsUtilService.getUserLocation(user.getUserId());
					user.addToVisitedLocations(visitedLocation);
					return visitedLocation;

				}
			});
		}
		//allowed to catch all results
		try {
			List<Future<VisitedLocation>> futures = executor.invokeAll(tasks);

		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			executor.shutdown();
		}
	}


	/**
	 * Method which allowed to run calculate rewards in parallel with others methods
	 *
	 * @param user   - the User which one we want to calculate rewards
	 *
	 */
	public CompletableFuture<Void> asynchronCalculateRewards(User user){

		return CompletableFuture.runAsync(() -> rewardsService.calculateRewards(user));

	}




	/**
	 * Obtains a list of the 5 closest attractions according to the user's location. The number of
	 * attractions is set by "numberOfAttractionsWanted".
	 *
	 * @param numberOfAttractionsWanted   - the number of attraction wanted (for the moment lock at 5)
	 * @param visitedLocation - The location of the user
	 *
	 * @return - A list of AttractionWithDistanceFromUser objects
	 */

	public AttractionWithDistanceFromUser getNearByAttractions(VisitedLocation visitedLocation, int numberOfAttractionsWanted, User user) {

		AttractionWithDistanceFromUser nearbyAttractions = new AttractionWithDistanceFromUser();

		try{
			nearbyAttractions = gpsUtilService.getNearbyAttraction(visitedLocation.location, numberOfAttractionsWanted, user);

			logger.debug("The nearby attractions were found");
		}
		catch (Exception ex)
		{
			logger.error("Error fetching the list of nearby attractions");
		}

		return nearbyAttractions;
	}

	/**
	 * Obtains a list of the last location of all users.
	 *
	 * @return - A list with user's id and their locations
	 */
	public Map<String, Location> getAllUserLocations() {
		Map<String, Location> usersLocations= new HashMap<String, Location>();

		try{
			List<User> users = getAllUsers();

			for (User user : users) {
				usersLocations.put(user.getUserId().toString(), user.getLastVisitedLocation().location);
			}
			logger.debug("The location of all users were find");
		}
		catch(Exception ex){
			logger.error("The location of all users failed");
		}

		return usersLocations;
	}

	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				tracker.stopTracking();
			}
		});
	}

	/**********************************************************************************
	 * Methods Below: For Internal Testing
	 **********************************************************************************/
	private static final String tripPricerApiKey = "test-server-api-key";

	public void setInternalUserMap(Map<String, User> internalUserMap) {
		this.internalUserMap = internalUserMap;
	}

	// Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
	private Map<String, User> internalUserMap = new HashMap<>();
	private void initializeInternalUsers() {
		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000";
			String email = userName + "@tourGuide.com";
			User user = new User(UUID.randomUUID(), userName, phone, email);
			generateUserLocationHistory(user);

			internalUserMap.put(userName, user);
		});
		logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}

	private void generateUserLocationHistory(User user) {
		IntStream.range(0, 3).forEach(i-> {
			user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
		});
	}

	private double generateRandomLongitude() {
		double leftLimit = -180;
		double rightLimit = 180;
		return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}

	private double generateRandomLatitude() {
		double leftLimit = -85.05112878;
		double rightLimit = 85.05112878;
		return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}

	private Date getRandomTime() {
		LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
		return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
	}


}
