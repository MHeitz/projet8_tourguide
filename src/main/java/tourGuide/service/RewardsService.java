package tourGuide.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.user.User;
import tourGuide.user.UserReward;

@Service
public class RewardsService {

	private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

	// proximity in miles
	private final int defaultProximityBuffer = 2000000000;
	private int proximityBuffer = defaultProximityBuffer;
	private final int attractionProximityRange = 200;
	private final GpsUtil gpsUtil;
	private final RewardCentral rewardsCentral;

	public RewardsService(GpsUtil gpsUtil, RewardCentral rewardCentral) {
		this.gpsUtil = gpsUtil;
		this.rewardsCentral = rewardCentral;
	}

	public void setProximityBuffer(int proximityBuffer) {
		this.proximityBuffer = proximityBuffer;
	}

	public void setDefaultProximityBuffer() {
		proximityBuffer = defaultProximityBuffer;
	}


	/**
	 * Calculate rewards points for a user.
	 *
	 * @param user - A User object
	 */
	public void calculateRewards(User user) {
		List<VisitedLocation> userLocations = user.getVisitedLocations();
		List<Attraction> attractions = gpsUtil.getAttractions();

		for (VisitedLocation visitedLocation : userLocations) {
			for (Attraction attraction : attractions) {
				if (user.getUserRewards().stream()
						.filter(r -> r.attraction.attractionName.equals(attraction.attractionName)).count() == 0) {
					if (nearAttraction(visitedLocation, attraction)) {
						user.addUserReward(new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user)));
					}
				}
			}
		}

	}

	/**
	 * Calculate rewards points for a list of users.
	 *
	 * @param users - A list of User object
	 */
	public void calculateAllRewards(List<User> users) {

		//Allowed to run pool of 100 threads
		final ExecutorService executor = Executors.newFixedThreadPool(1500);
		List<Attraction> attractions = gpsUtil.getAttractions();


		List<Callable<String>> tasks = new ArrayList<Callable<String>>();

		for (User user : users) {
			System.out.println("addition of the callable");
			tasks.add(new Callable<String>() {

				public String call() throws Exception {

					List<VisitedLocation>userLocations = user.getVisitedLocations();

					for (VisitedLocation visitedLocation : userLocations) {
						for (Attraction attraction : attractions) {
							if (user.getUserRewards().stream()
									.filter(r -> r.attraction.attractionName.equals(attraction.attractionName)).count() == 0) {
								if (nearAttraction(visitedLocation, attraction)) {
									user.addUserReward(new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user)));

								}
							}
						}
					}
					return "Ok";
				}
			});
		}
		try {
			List<Future<String>> futures = executor.invokeAll(tasks);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		finally {

			executor.shutdown();
		}
	}





	public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
		return getDistance(attraction, location) > attractionProximityRange ? false : true;
	}

	private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {

		return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
	}

	public int getRewardPoints(Attraction attraction, User user) {
		return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
	}

	public double getDistance(Attraction loc1, Location loc2) {
		double lat1 = Math.toRadians(loc1.latitude);
		double lon1 = Math.toRadians(loc1.longitude);
		double lat2 = Math.toRadians(loc2.latitude);
		double lon2 = Math.toRadians(loc2.longitude);

		double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
				+ Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

		double nauticalMiles = 60 * Math.toDegrees(angle);
		return STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;

	}



}
