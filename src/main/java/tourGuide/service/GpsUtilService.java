package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.model.AttractionWithDistanceFromUser;
import tourGuide.user.User;
import java.util.UUID;

public interface GpsUtilService {

    double convertDegreesToRadians(double degrees);

    double calculateDistanceBetweenTwoPoints(Attraction attraction, Location location);

     AttractionWithDistanceFromUser getNearbyAttraction(Location location, int numberOfAttractionWanted, User user);

    VisitedLocation getUserLocation(UUID userId);

    void sleep();
}
