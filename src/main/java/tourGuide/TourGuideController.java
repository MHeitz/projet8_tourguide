package tourGuide;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonView;
import gpsUtil.location.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.jsoniter.output.JsonStream;

import gpsUtil.location.VisitedLocation;
import tourGuide.model.*;
import tourGuide.service.TourGuideService;
import tourGuide.service.UserService;
import tourGuide.user.User;
import tripPricer.Provider;

@RestController
public class TourGuideController {

	@Autowired
	TourGuideService tourGuideService;
    @Autowired
    UserService userService;
	
    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }


    @RequestMapping("/getLocation")
    public ResponseEntity getLocation(@RequestParam String userName) {

        if(getUser(userName) != null)
        {
            VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
            return new ResponseEntity<>(visitedLocation.location, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }


    @RequestMapping("/getNearbyAttractions")
    @JsonView(View.NearbyAttractionFilter.class)
    public ResponseEntity getNearbyAttractions(@RequestParam String userName) {

        if(getUser(userName) != null){

            User user = tourGuideService.getUser(userName);
            VisitedLocation visitedLocation = tourGuideService.getUserLocation(user);
            double longitude = visitedLocation.location.longitude;
            double latitude = visitedLocation.location.latitude;
            LocationDTO locationDTO = new LocationDTO();
            locationDTO.setLongitude(longitude);
            locationDTO.setLatitude(latitude);
            user.setLastLocation(locationDTO);

            AttractionWithDistanceFromUser attractionWithDistanceFromUser = tourGuideService.getNearByAttractions(visitedLocation, 5, user);
            attractionWithDistanceFromUser.setUser(user);

            return new ResponseEntity<>(attractionWithDistanceFromUser, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping("/getRewards") 
    public ResponseEntity getRewards(@RequestParam String userName) {

        if(getUser(userName) != null){
            return new ResponseEntity<>(JsonStream.serialize(tourGuideService.getUserRewards(getUser(userName))), HttpStatus.OK);
        }
    	else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping("/getAllCurrentLocations")
    public ResponseEntity getAllCurrentLocations() {

        if( tourGuideService.getAllUserLocations() != null){

            return new ResponseEntity<>(tourGuideService.getAllUserLocations(), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }


    /**
     * Modification of the controller getTripDeals,
     * because an error occurred with the older version,
     * nullPointerException about the user.
     *
     * @param userName   - the User's name
     */
    @RequestMapping("/getTripDeals")
    public ResponseEntity getTripDeals(@RequestParam String userName) {

        if(getUser(userName) != null){
            User user = tourGuideService.getUser(userName);
            return new ResponseEntity<>(tourGuideService.getTripDeals(user), HttpStatus.OK);
        }
       else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     *This controller allowed to post new user's preferences
     *
     * @param userName   - the User's name
     * @param userPreferences - the user's preferences
     *
     */
    @PostMapping("/postUserPreferences")
    public ResponseEntity<HttpStatus> postNewUserPreferences (@RequestBody UserPreferencesDTO userPreferences, @RequestParam(name = "userName") String userName)
    {
        if(userService.saveNewUserPreference(userPreferences, userName))
        {
            return new ResponseEntity<HttpStatus>(HttpStatus.CREATED);
        }
        return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
    }

    
    private User getUser(String userName) {
    	return tourGuideService.getUser(userName);
    }
   

}